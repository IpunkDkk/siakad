# Sistem Informasi Akademik

Sistem Informasi Akademik adalah proyek berbasis web yang dikembangkan menggunakan Laravel, Filament, Tailwind CSS, dan Flowbite. Proyek ini dirancang untuk menyediakan solusi informatif dan efisien untuk manajemen data akademik di institusi pendidikan.

## Cara Berkontribusi

Terima kasih atas ketertarikan Anda untuk berkontribusi pada proyek ini! Kami menyambut kontribusi dari komunitas.

Berikut langkah-langkah umum untuk berkontribusi:

1. Fork proyek ini (klik tombol "Fork" di kanan atas halaman).
2. Clone repositori yang telah Anda fork ke lokal mesin Anda.

    ```bash
    git clone https://gitlab.com/IpunkDkk/siakad.git
    ```

3. Buat branch baru untuk fitur atau perbaikan.

    ```bash
    git checkout -b fitur-anda
    ```

4. Lakukan perubahan yang diperlukan dan commit perubahan Anda.

    ```bash
    git add .
    git commit -m "Menambahkan fitur baru"
    ```

5. Push branch Anda ke GitHub.

    ```bash
    git push origin fitur-anda
    ```

6. Buat pull request. Pergi ke repositori Anda di GitHub, klik tombol "Compare & pull request", dan ikuti petunjuk selanjutnya.

**Catatan:** Pastikan untuk memperbarui repositori Anda lokal sebelum membuat pull request untuk menghindari konflik.

## Pedoman Kontribusi

Project ini menggunakan pola repository service pattern yang mana sudah dijesalkan di [README.md](README.md)

## Laporkan Masalah

Jika Anda menemui masalah atau bug, silakan buat _issue_ di [halaman masalah](https://gitlab.com/IpunkDkk/siakad/issues) kami.

## Kontak

Jika Anda memiliki pertanyaan atau memerlukan bantuan lebih lanjut, hubungi kami melalui [email](mailto:ipunka37@gmail.com) atau [telegram](https://t.me/agung_dkk). Terima kasih atas kontribusi Anda!
