# Sistem Informasi Akademik

Sistem Informasi Akademik adalah proyek berbasis web yang dikembangkan menggunakan Laravel, Filament, Tailwind CSS, dan Flowbite. Proyek ini dirancang untuk menyediakan solusi informatif dan efisien untuk manajemen data akademik di institusi pendidikan.

## Fitur Utama

-   Manajemen Siswa: Tambah, edit, dan hapus informasi siswa.
-   Manajemen Mata Pelajaran: Kelola daftar mata pelajaran dan detailnya.
-   Nilai: Tetapkan nilai kepada siswa.
-   Laporan Akademik: Lihat dan cetak laporan akademik siswa.

## Teknologi yang Digunakan

-   **[Laravel](https://laravel.com)**: Framework PHP yang kuat untuk pengembangan web.
-   **[Filament](https://filamentphp.com/)**: Kumpulan komponen full-stack yang indah. Titik awal yang sempurna untuk aplikasi Anda selanjutnya.
-   **[Tailwind CSS](https://tailwindcss.com/)**: Kerangka desain CSS yang ringan dan dapat disesuaikan.
-   **[Flowbite](https://flowbite.com/)**: Komponen UI untuk Tailwind CSS yang mempercepat pengembangan.

## Instalasi

1. Pastikan Anda memiliki [Composer](https://getcomposer.org/) terinstal.
2. Clone repositori ini: `git clone https://gitlab.com/IpunkDkk/siakad.git`
3. Masuk ke direktori proyek: `cd siakad`
4. Salin file `.env.example` menjadi `.env` dan atur konfigurasi database.
5. Jalankan perintah `composer install` untuk menginstal dependensi.
6. Jalankan perintah `npm install` untuk menginstal dependensi.
   dependensi.
7. Jalankan perintah `php artisan key:generate` untuk menghasilkan kunci aplikasi.
8. Jalankan perintah `php artisan migrate` untuk membuat tabel database.
9. Jalankan perintah `npm run dev`.
10. Jalankan perintah `php artisan serve` untuk menjalankan server pengembangan.

## Struktur Repositori

Pola Repositori digunakan untuk memisahkan logika bisnis dari penyimpanan data. Struktur repositori proyek ini dibuat dengan mempertimbangkan pola tersebut:

-   **app/Repositories**: Berisi semua kelas repositori.
-   **app/Services**: Berisi layanan dan logika bisnis.
-   **app/Models**: Berisi model Eloquent untuk kueri ke database.
-   **database/migrations**: Berisi file migrasi database.
-   **database/seeders**: Berisi pengisi data awal.

## Kontribusi

Kami mengundang kontribusi dari komunitas. Silakan lihat [Panduan Kontribusi](CONTRIBUTING.md) untuk informasi lebih lanjut.

## Lisensi

Proyek ini dilisensikan di bawah Lisensi MIT - lihat file [LICENSE](LICENSE) untuk detailnya.
